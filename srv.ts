/**
 * This module contains all the services consumed by the application. It acts
 * as the source of truth regarding the services used and their version.
 */

import Service, { VersioningMode, ResponseFormat } from './lib'

export const pokemon = new Service(
    'https://pokeapi.co/api',
    'v2',
    VersioningMode.URL,
    ResponseFormat.JSON,
);
