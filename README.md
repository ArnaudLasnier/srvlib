# `srvlib`: Managing Microservices and their Versions

## Main Goal

`srvlib` aims at providing a thin wrapper around the Fetch API for a better
microservices management. Let's compare:

``` js
// Raw Fetch API:
const dittoPokemon = await fetch('https://pokeapi.co/api/v2/pokemon/ditto/');

// Now with `srvlib`:
const dittoPokemon = await srv.pokemon.fetch('/pokemon/ditto/');
```

Here `srv` is simply a plain JavaScript object whose keys are the name of the
services and whose values are instances of the `Service` class. You can think
of it as the source of truth regarding the services your application is using
(and their version).

The `Service` class provides a `fetch` method similar to [the one provided
natively by your browser](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API).

Just by looking at this snippet, you can see that the information regarding the
service base URL and its version tag is encapsulated in the `Service` instance
state.

## But why ?

Well, the primary goal is to avoid things like this:

``` js
const dittoPokemon = await fetch(`${POKEMON_SERVICE_BASE_URL}/${POKEMON_SERVICE_VERSION}/pokemon/ditto/`);
```

or worse:

``` js
const dittoPokemon = await fetch(`${POKEMON_SERVICE_BASE_URL}/v2/pokemon/ditto/`);
```

These examples do not look beautiful, they are prone to errors and subjects to
code duplication.

The second example is even worse.

See how the `v2` is hard coded here ? When doing microservices, it is important
to explicitly track which version of the service the client is consuming and
to avoid ending up with a client consuming multiple versions of the same
service. This simplifies depreacting microservice versions that are not used
anymore.

`srvlib` helps with that.

## Wait, there is more!

Some teams like to avoid hardcoding version tags like `v2` in their URL to
follow REST principles. In a REST style, resources are considered durable in
their nature and the HTTP mechanisms are prefered (HTTP content negociation
here).

`srvlib` allows you to do just that. By decoupling the base URL and the version
tag, you are able to choose which versioning mode you want:

- either the `AcceptHeader` mode that leverages the standard HTTP `Accept`
  header and [RFC 4288](https://tools.ietf.org/html/rfc4288#section-3.2),
- or the `URL` mode that appends the version tag to the base URL.

Let's have a look:

``` js
import Service, { VersioningMode, ResponseFormat } from './lib'

export const pokemonService = new Service(
    'https://pokeapi.co/api',
    'v2',
    VersioningMode.URL,
    ResponseFormat.JSON,
);

pokemonService.fetch('/pokemon/ditto/');

// Endpoint: https://pokeapi.co/api/v2/pokemon/ditto/
// Accept Header: application/json
```

vs

``` js
import Service, { VersioningMode, ResponseFormat } from './lib'

export const pokemonService = new Service(
    'https://pokeapi.co/api',
    'v2',
    VersioningMode.AcceptHeader,
    ResponseFormat.JSON,
    'pokeapi'
);

pokemonService.fetch('/pokemon/ditto/');

// Endpoint: https://pokeapi.co/api/pokemon/ditto/
// Accept Header: application/vnd.pokeapi.v2+json
```

## Disclaimer

This library is still a proof of concept. I do not use it (yet!) in production
but plan to in the near future.

You can run the demo with:

``` bash
npm install
make test
```

`make test` wraps `npx ts-node test.ts`.
