/**
 * A proof of concept.
 */

import * as srv from './srv'

async function run(): Promise<void> {
    try {
        const response = await srv.pokemon.fetch('/pokemon/ditto/');
        const dittoPokemon = await response.json();

        const got = dittoPokemon.abilities[0].ability;
        const expected = {
            name: 'imposter',
            url: 'https://pokeapi.co/api/v2/ability/150/',
        };

        console.log('got =', got);

        const matchExpected = JSON.stringify(got) === JSON.stringify(expected);

        console.log('matchExpected =', matchExpected);
    } catch (error) {
        console.error(error);
    }
}

run();
