// using `node-fetch` for development purposes:
import fetch, { Headers } from 'node-fetch';

/**
 * Join two paths that may (or may not) end (or begin) with a "/".
 * @param startPath
 * @param endPath
 */
function joinPaths(startPath: string, endPath: string): string {
    if (startPath.endsWith('/')) {
        if (endPath.startsWith('/')) {
            return startPath.substring(1) + endPath;
        } else {
            return startPath + endPath;
        }
    } else {
        if (endPath.startsWith('/')) {
            return startPath + endPath;
        } else {
            return startPath + '/' + endPath;
        }
    }
}

/**
 * Join a URL and a path.
 *
 * This function is needed because `new URL(input, base)` gets rid of the path
 * of the `base`. This function correctly resolves the path.
 *
 * @param url A URL that ends at the end of the host or in the path name.
 * @param remainingPartOfPath A full or partial path that may or may not begin
 * with "/".
 */
function joinURLAndPath(url: string, remainingPartOfPath: string): string {
    const parsedURLEndingInPath = new URL(url);

    const startPath = parsedURLEndingInPath.pathname;
    let fullPath = joinPaths(startPath, remainingPartOfPath);

    const baseURL = parsedURLEndingInPath.protocol
        + '//'
        + parsedURLEndingInPath.host;

    return new URL(fullPath, baseURL).toString();
}

export enum VersioningMode {
    AcceptHeader,
    URL,
}

export enum ResponseFormat {
    JSON,
    XML,
    CSV,
    YAML,
}

/**
 * A `Service` offers a `fetch` method similar to the standard Fetch API but
 * with a resolved base URL and a service versioning mechanism:
 * - either using a modified base URL (e.g. `https//api.example.com/v2`)
 * - or using the standard HTTP content negociation mechanisms (the default):
 * `Accept: vnd.example.v2+json`.
 */
export default class Service {
    baseURL: string;
    versionTag: string;
    versioningMode: VersioningMode;
    responseFormat: ResponseFormat;
    vendorCode: string;

    /**
     * The `Service` constructor.
     * @param baseURL The base URL **minus the `versionTag`**.
     * @param versionTag The version tag: it can have any format: `v1`, `1.0`,
     * etc. It will automatically be appended to the `baseURL` when
     * `versioningMode` is set to `VersioningMode.URL`.
     * @param versioningMode Either `URL` or `AcceptHeader`.
     * @param responseFormat The supported response format: `JSON`, `XML`,
     * `CSV` or `YAML`.
     * @param vendorCode The vendor code used for a custom HTTP Accept Header.
     * Must be provided unless `versioningMode` is set to `URL`.
     */
    constructor(
        baseURL: string,
        versionTag: string,
        versioningMode: VersioningMode = VersioningMode.AcceptHeader,
        responseFormat: ResponseFormat = ResponseFormat.JSON,
        vendorCode?: string,
    ) {
        // Do not accept nullable string for `baseURL`:
        if (baseURL === null || baseURL === undefined) {
            throw new TypeError('Invalid service base URL');
        }

        // Do not accept nullable string for `versionTag`:
        if (versionTag === null || versionTag === undefined) {
            throw new TypeError('Invalid service version tag');
        }

        const parsedBaseURL = new URL(baseURL);
        if (
            parsedBaseURL.username !== ''
            || parsedBaseURL.password !== ''
            || parsedBaseURL.search !== ''
            || parsedBaseURL.hash !== ''
        ) {
            throw new TypeError('Invalid service base URL');
        }

        // TODO: enforce rules for the versionTag (e.g. no "/")

        // TODO: expose a method that defines the regexp and can be overriden
        // by child classes.
        if (vendorCode !== undefined && !vendorCode.match(/^[a-z_-]{3,15}$/)) {
            throw new TypeError('Invalid service vendor code');
        }

        this.baseURL = baseURL;
        this.versionTag = versionTag;
        this.versioningMode = versioningMode;
        this.vendorCode = vendorCode;
        this.responseFormat = responseFormat;
    }

    get responseFormatString(): string {
        if (this.responseFormat === ResponseFormat.JSON) {
            return 'json';
        } else if (this.responseFormat === ResponseFormat.XML) {
            return 'xml';
        } else if (this.responseFormat === ResponseFormat.CSV) {
            return 'csv';
        } else if (this.responseFormat === ResponseFormat.YAML){
            return 'yaml';
        } else {
            throw TypeError('A corresponding string has not been defined for this response format');
        }
    }

    get acceptHeaderUsed(): string {
        if (this.versioningMode === VersioningMode.URL) {
            // In this case, the "Accept" HTTP header is the MIME type:
            if (this.responseFormat === ResponseFormat.JSON) {
                return 'application/json';
            } else if (this.responseFormat === ResponseFormat.XML) {
                return 'application/xml, text/xml';
            } else if (this.responseFormat === ResponseFormat.CSV) {
                return 'text/csv';
            } else if (this.responseFormat === ResponseFormat.YAML){
                return 'text/yaml';
            } else {
                throw TypeError('A MIME type has not been defined for this response format');
            }
        }

        if (this.versioningMode === VersioningMode.AcceptHeader) {
            // In this case, we cannot use the MIME type as the "Accept" HTTP
            // header and must use a custom (vendor) Accept header instead:
            return `application/vnd.${this.vendorCode}.${this.versionTag}+${this.responseFormatString}`;
        }
    }

    get serviceURL(): string {
        return joinURLAndPath(this.baseURL, this.versionTag);
    }

    /**
     * Wrapper around the Fetch API: the base URL and the versioning are
     * already resolved.
     * @param urlStartingInPath URL of the service, starting in the path.
     * @param init An optional `RequestInit` object, as expected by the Fetch
     * API, that will be merged with the service configuration.
     */
    async fetch(urlStartingInPath: string, init?: RequestInit): Promise<Response> {
        const headers = new Headers([
            ['Accept', this.acceptHeaderUsed],
        ]);
        const fetchURL = joinURLAndPath(this.serviceURL, urlStartingInPath);
        Object.assign({ headers }, init);
        const response = await fetch(fetchURL, init);
        return response;
    }
}
