test: ## Test the library (proof of concept).
	npx ts-node test.ts

.PHONY: help
help: ## Show the documentation of the available commands (same as make).
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
